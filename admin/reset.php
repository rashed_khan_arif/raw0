<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- start: Meta -->
        <meta charset="utf-8">
        <title>Login</title>
        <meta name="description" content="Bootstrap Metro Dashboard">
        <meta name="author" content="Dennis Ji">
        <meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
        <!-- end: Meta -->

        <!-- start: Mobile Specific -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- end: Mobile Specific -->

        <!-- start: CSS -->
        <link id="bootstrap-style" href="design/css/bootstrap.min.css" rel="stylesheet">
        <link href="design/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link id="base-style" href="design/css/style.css" rel="stylesheet">
        <link id="base-style-responsive" href="design/css/style-responsive.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
        <!-- end: CSS -->


        <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                <link id="ie-style" href="css/ie.css" rel="stylesheet">
        <![endif]-->

        <!--[if IE 9]>
                <link id="ie9style" href="css/ie9.css" rel="stylesheet">
        <![endif]-->

        <!-- start: Favicon -->
        <link rel="shortcut icon" href="design/img/favicon.ico">
        <!-- end: Favicon -->
        <style type="text/css">
            body { background: url(design/img/bg-login.jpg) !important; }
        </style>
    </head>

    <body>
        <div class="container-fluid-full">
            <div class="row-fluid">

                <div class="row-fluid">
                    <div class="login-box">
                        <div class="icons">
                            <?php
                            $msg = $this->session->userdata('message');
                            if ($msg) {
                                ?>
                                <h2 style="color:red; font-size: 19px"><?php
                                    echo $msg;
                                    $this->session->unset_userdata('message');
                                    ?></h2>
<?php } ?>
                        </div>

                        <form class="form-horizontal" action="Admin_login/check_email_add" method="post">
                            <fieldset>

                                <div class="input-prepend" title="Email Address">
                                    <span class="add-on"><i class="halflings-icon user"></i></span>
                                    <input class="input-large span10" name="email_address" id="username" type="email" placeholder="type email address"/>
                                </div>
                                <div class="clearfix"></div>

                                <div class="button-login">	
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                                <div class="clearfix"></div>
                        </form>
                        <hr>

                    </div><!--/span-->
                </div><!--/row-->


            </div><!--/.fluid-container-->

        </div><!--/fluid-row-->

        <!-- start: JavaScript-->

        <script src="design/js/jquery-1.9.1.min.js"></script>
        <script src="design/js/jquery-migrate-1.0.0.min.js"></script>

        <script src="design/js/jquery-ui-1.10.0.custom.min.js"></script>

        <script src="design/js/jquery.ui.touch-punch.js"></script>

        <script src="design/js/modernizr.js"></script>

        <script src="design/js/bootstrap.min.js"></script>

        <script src="design/js/jquery.cookie.js"></script>

        <script src='design/js/fullcalendar.min.js'></script>

        <script src='design/js/jquery.dataTables.min.js'></script>

        <script src="design/js/excanvas.js"></script>
        <script src="design/js/jquery.flot.js"></script>
        <script src="design/js/jquery.flot.pie.js"></script>
        <script src="design/js/jquery.flot.stack.js"></script>
        <script src="design/js/jquery.flot.resize.min.js"></script>

        <script src="design/js/jquery.chosen.min.js"></script>

        <script src="design/js/jquery.uniform.min.js"></script>

        <script src="design/js/jquery.cleditor.min.js"></script>

        <script src="design/js/jquery.noty.js"></script>

        <script src="design/js/jquery.elfinder.min.js"></script>

        <script src="design/js/jquery.raty.min.js"></script>

        <script src="design/js/jquery.iphone.toggle.js"></script>

        <script src="design/js/jquery.uploadify-3.1.min.js"></script>

        <script src="design/js/jquery.gritter.min.js"></script>

        <script src="design/js/jquery.imagesloaded.js"></script>

        <script src="design/js/jquery.masonry.min.js"></script>

        <script src="design/js/jquery.knob.modified.js"></script>

        <script src="design/js/jquery.sparkline.min.js"></script>

        <script src="design/js/counter.js"></script>

        <script src="design/js/retina.js"></script>

        <script src="design/js/custom.js"></script>
        <!-- end: JavaScript-->

    </body>
</html>
