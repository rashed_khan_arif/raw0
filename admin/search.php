<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Manage Product</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
         <?php
    $msg = $this->session->userdata('pmsg');
    if ($msg) {
        ?>
        <span style="color: red;font-size: 20px">
            <?php
            echo $msg;
            $this->session->unset_userdata('pmsg')
            ?>
        </span>
    <?php } ?>
        <form action="<?php echo base_url()?>super_admin/search_product" method="get">
        <input type="text" name="search" required placeholder="Enter Product Code">
        
          <h2 class="pull-right"><span class="break"></span><a href="super_admin/add_product" class="btn btn-info">Add New Product</a>
          <h2 class="pull-right"><span class="break"></span><a href="super_admin/manage_product" class="btn btn-info">Manage Product</a>
          </h2></form>
            <table class="table table-responsive table-bordered ">
            
                <thead class="text-center">
                <th>Id</th>
            
                <th>Catagory</th>
                <th>Sub Id</th>
                <th>Brand Id</th>
                <th>Product Name</th>
                <th>Code</th>
                <th>Price</th>
                <th>Availability</th>
                <th>Image</th>
                <th>Feature</th>
                <th>Status</th>
                <th>Action</th>
                </thead>
                <tbody>
                    <?php foreach ($select_product as $v_product) { ?>
                        <tr>
                            <td><?php echo $v_product->product_id ?></td>
                           
                            <td><?php echo $v_product->cat_name ?></td>
                            <td><?php echo $v_product->sub_id;?></td>
                            <td><?php echo $v_product->brand_id ?></td>
                            <td><?php echo $v_product->product_name ?></td>
                            <td><?php echo $v_product->product_code ?></td>
                            <td><?php echo $v_product->product_price ?></td>
                            <td><?php echo $v_product->availability ?></td>

                            <td><img src="<?php echo base_url() . $v_product->product_image ?>" width="50" height="50"></td>
                            <td>
                                <?php
                                if ($v_product->featured_product == 1) {
                                    echo 'Featured';
                                } else {
                                    echo 'No';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($v_product->pub_status == 1) {
                                    echo 'Published';
                                } else {
                                    echo 'Un Published';
                                }
                                ?>
                            </td>
                            <td>   <?php
                                if ($v_product->pub_status == 1) {
                                    ?>
                                    <a class="btn btn-default" href="<?php echo base_url(); ?>super_admin/un_published_product/<?php echo $v_product->product_id ?>">
                                        <i class="halflings-icon off" title="Published"></i>
                                    </a>
                                <?php } else { ?>
                                    <a class="btn btn-hover" href="<?php echo base_url(); ?>super_admin/published_product/<?php echo $v_product->product_id ?>">
                                        <i class="halflings-icon ok" title="Un Published"></i>
                                    </a>
                                <?php } ?>
                                <a class="btn btn-info" href="<?php echo base_url(); ?>super_admin/edit_product/<?php echo $v_product->product_id ?>">
                                    <i class="halflings-icon edit" title="Edit"></i>
                                </a>
                                <script type="text/javascript">
                                    function chkdelete() {
                                        var chk = confirm('Are you sure ?');
                                        if (chk) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    }
                                </script>
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>super_admin/delete_product/<?php echo $v_product->product_id ?>">
                                    <i class="halflings-icon remove-sign" title="Delete" onclick="return chkdelete();"></i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
<ul class="pagination-centered">
                <?php
                echo $this->pagination->create_links();
                ?>
            </ul>
        </div>
    </div><!--/span-->

</div><!--/row-->



