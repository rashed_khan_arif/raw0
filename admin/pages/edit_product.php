<p class="panel">
    <?php
    $msg = $this->session->userdata('message');
    if ($msg) {
        ?>
        <span style="color: red; margin-left: 40%;font-size: 24px">
            <?php
            echo $msg;
            $this->session->unset_userdata('message')
            ?>
        </span>
    <?php } ?>
</p>
<script src="design/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var xmlhttp = false;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }

    function makerequest(given_value, objID)
    {
        //var obj = document.getElementById(objID);
        //alert(objID);
        serverPage = 'super_admin/sub_cat_check/' + given_value;
        // alert(serverPage);
        xmlhttp.open("GET", serverPage);
        xmlhttp.onreadystatechange = function ()
        {
            //alert(xmlhttp.readyState);
            //alert(xmlhttp.status);
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById(objID).innerHTML = xmlhttp.responseText;
                //document.getElementById(objcw).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.send(null);
    }
</script>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Update Your Product</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" name="product" action="super_admin/update_product" method="post" enctype="multipart/form-data">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="box1">Product Name </label>
                        <div class="controls">
                            <input type="hidden" value="<?php echo $select_product_by_id->product_id ?>" name="product_id" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4" >
                            <input type="text" name="product_name" value="<?php echo $select_product_by_id->product_name ?>" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4" >
<!--                            <p class="help-block">Start typing to activate auto complete!</p>-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="box1">Product Description</label>
                        <div class="controls">
                            <textarea class="cleditor" name="product_des" id="textarea2" rows="3">
                                <?php echo $select_product_by_id->product_des ?>
                            </textarea>
                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="box1">Product Code </label>
                        <div class="controls">
                            <input type="text" value="<?php echo $select_product_by_id->product_code ?>" name="product_code" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4"
                        </div>
                    </div>   
                    </div>   
                    <div class="control-group">
                        <label class="control-label" for="box1">Product Price</label>
                        <div class="controls">

                            <input type="text" value="<?php echo $select_product_by_id->product_price ?>" name="product_price" placeholder="type price amount or anything" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4"
                        </div>
                    </div>   
                    </div>   
                    <div class="control-group">
                        <label class="control-label" for="box1">Availability</label>
                        <div class="controls">
                            <input type="text" name="availability" value="<?php echo $select_product_by_id->availability ?>" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4"
                        </div>
                    </div>   
                    </div>   
                    <div class="control-group">
                        <label class="control-label" for="box1">Product Status</label>
                        <div class="controls">
                            <input type="text" name="status" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4"
                        </div>
                    </div>   
                    </div>   
                    <div class="control-group">
                        <label class="control-label" for="box1">Condition</label>
                        <div class="controls">
                            <textarea class="cleditor" name="condition" id="textarea2" rows="3">
                                <?php echo $select_product_by_id->condition ?>
                            </textarea>
                        </div>
                    </div>   


                    <div class="control-group">
                        <label class="control-label">Featured Product</label>
                        <div class="controls">
                            <label class="radio">
                                <?php if ($select_product_by_id->featured_product == 1) { ?>
                                    <input type="checkbox" name="featured_product" id="optionsRadios1">
                                <?php } ?>
                                <?php
                                if ($select_product_by_id->featured_product == 1) {
                                    echo 'Yes';
                                } else {
                                    ?> 
                                    <input type="checkbox" name="featured_product" id="optionsRadios1">
                                    <?php
                                    echo 'No ....Click to Yes';
                                }
                                ?>

                            </label> 
                        </div>
                    </div> 

                    <div class="control-group">
                        <label class="control-label">Product Catagory</label> 
                        <div class="controls">
                            <select id="selectError" required  onchange="makerequest(this.value, 'msg')" name="cat_id">
                                <option value="">select catagory</option>
                                <?php foreach ($select_catagory as $v_catagory) { ?>
                                    <option value="<?php echo $v_catagory->cat_id; ?>"><?php echo $v_catagory->cat_name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div> 
                   
                    <div class="control-group">
                        <label class="control-label" required style="color: green"> Sub Catagory</label>       
                        <div class="controls">
                            <select  id="msg" name="sub_id">   
                            <option value=""></option>
                                <?php foreach ($select_sub_catagory as $v_scat){?>
                                
                                <option value="<?php echo $v_scat->sub_id?>"><?php echo $v_scat->sub_name?></option>
                                <?php }?>
                                
                            </select>
                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label">Product Brand</label> 
                        <div class="controls">
                            <select id="selectError" required  name="brand_id">
                                <option value="">select brand</option>
                                <?php foreach ($select_brand as $v_brand) { ?>
                                    <option value="<?php echo $v_brand->brand_id; ?>"><?php echo $v_brand->brand_name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div> 
                    <!--                    <div class="control-group">
                                            <label class="control-label" for="date01">Date input</label>
                                            <div class="controls">
                                                <input type="text" class="input-xlarge datepicker" id="date01" value="02/16/12">
                                            </div>
                                        </div>-->
                    <div class="control-group">
                        <label class="control-label" for="fileInput">Product Image</label>
                        <div class="controls">
                            <input class="input-file uniform_on" name="product_image" id="fileInput" type="file">
                            <div class="clearfix"></div>
                            <img src="<?php echo base_url() . $select_product_by_id->product_image ?>" width="100" heigth="100">
                            <p class="help-block" style="color: green">Image must be Width 253px and height 188px and size maximum 150 kb</p>
                        </div>   
                    </div>  
                    <div class="control-group">
                        <label class="control-label" for="fileInput">Product Image For Zoom</label>
                        <div class="controls">
                            <input class="input-file uniform_on" name="product_image2" id="fileInput" type="file">
                            <div class="clearfix"></div>
                            <img src="<?php echo base_url() . $select_product_by_id->product_image2 ?>" width="100" heigth="100">
                    </div>   
                    <div class="control-group">
                        <label class="control-label">Publication status</label> 
                        <div class="controls">
                            <select id="selectError" name="pub_status">
                                <option>select publication status</option>
                                <option value="1">Published</option>
                                <option value="0">Un_Published</option>
                            </select>
                        </div>
                    </div>  

                    <!--                    <div class="control-group hidden-phone">
                                            <label class="control-label" for="textarea2">Textarea WYSIWYG</label>
                                            <div class="controls">
                                                <textarea class="cleditor" id="textarea2" rows="3"></textarea>
                                            </div>
                                        </div>-->
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->
<script type="text/javascript">
    document.forms['product'].elements['pub_status'].value = '<?php echo $select_product_by_id->pub_status ?>'
    document.forms['product'].elements['cat_id'].value = '<?php echo $select_product_by_id->cat_id ?>'
    document.forms['product'].elements['sub_id'].value = '<?php echo $select_product_by_id->sub_id ?>'
    document.forms['product'].elements['brand_id'].value = '<?php echo $select_product_by_id->brand_id ?>'
</script>