<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Manage Catagory</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-responsive table-bordered ">
                                        <h2 class="pull-right"><i class="halflings-icon edit"></i><span class="break"></span><a href="<?php echo base_url()?>super_admin/add_page">Add New Page</a></h2>
                <thead class="text-center">
                <th>slider Id</th>
                <th>Title</th>
                <th>Product Title</th>
                <th>Image</th>
                <th>Publication Status</th>
                <th>Action</th>
                </thead>
                <tbody>
                    <?php foreach ($select_slider as $v_slider) { ?>
                        <tr>
                            <td><?php echo $v_slider->slider_id ?></td>
                            <td><?php echo $v_slider->title ?></td>
                            <td><?php echo $v_slider->p_title ?></td>
                            <td><img src="<?php echo base_url().$v_slider->p_image ?>" width="100" height="100"/></td>
                            <td>
                                <?php
                                if ($v_slider->pub_status == 1) {
                                    echo 'Active';
                                } else {
                                    echo 'In Active';
                                }
                                ?>
                            </td>
                            <td>   <?php
                                if ($v_slider->pub_status == 1) {
                                    ?>
                                    <a class="btn btn-default" href="<?php echo base_url(); ?>super_admin/un_published_slider/<?php echo $v_slider->slider_id ?>">
                                        <i class="halflings-icon off" title="Published"></i>
                                    </a>
                                <?php } else { ?>
                                    <a class="btn btn-hover" href="<?php echo base_url(); ?>super_admin/published_slider/<?php echo $v_slider->slider_id ?>">
                                        <i class="halflings-icon ok" title="Un Published"></i>
                                    </a>
                                <?php } ?>
                                <a class="btn btn-info" href="<?php echo base_url(); ?>super_admin/edit_slider/<?php echo $v_slider->slider_id?>">
                                    <i class="halflings-icon edit" title="Edit"></i>
                                </a>
                                <script type="text/javascript">
                                    function chkdelete() {
                                        var chk = confirm('Are you sure ?');
                                        if (chk) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    }
                                </script>
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>super_admin/delete_slider/<?php echo $v_slider->slider_id ?>">
                                    <i class="halflings-icon remove-sign" title="Delete" onclick="return chkdelete();"></i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>  

        </div>
    </div><!--/span-->

</div><!--/row-->



