<p class="panel">
    <?php
    $msg = $this->session->userdata('message');
    if ($msg) {
        ?>
        <span style="color: red; margin-left: 40%;font-size: 24px">
            <?php
            echo $msg;
            $this->session->unset_userdata('message')
            ?>
        </span>
    <?php } ?>
</p>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Edit Admin</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" name="admin" action="super_admin/update_admin" method="post" enctype="multipart/form-data">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="box1">Admin Name </label>
                        <div class="controls">
                            <input type="hidden" name="admin_id" value="<?php echo $select_one_admin->admin_id; ?>" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4" >
                                   <input type="text" name="admin_name" value="<?php echo $select_one_admin->admin_name; ?>" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4"> 
    

                        </div>
                    </div>
      
       <div class="control-group">
                        <label class="control-label" for="box1">Admin Email Address</label>
                        <div class="controls">
                      <input type="email" name="email_address" value="<?php echo $select_one_admin->email_address; ?>" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4"> 
    

                        </div>
                    </div>
      
                    <div class="control-group">
                        <label class="control-label">Access Level</label> 
                        <div class="controls">
                            <select id="selectError" name="access_level">
                                <option>select access level</option>
                                <option value="1">Full Access</option>
                                <option value="0">Medium Access</option>
                            </select>
                        </div>
                    </div>          
                   
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->



<div class="row-fluid sortable">
    <div class="box span12">
        
    </div><!--/span-->

</div>
<script type="text/javascript">
    document.forms['admin'].elements['access_level'].value = '<?php echo $select_one_admin->access_level; ?>';
</script>