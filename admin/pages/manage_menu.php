<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Manage Catagory</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-responsive table-bordered ">
                                        <h2 class="pull-right"><span class="break"></span><a href="<?php echo base_url()?>super_admin/add_menu" class="btn btn-info">Add New menu</a></h2>
                <thead class="text-center">
                <th>Menu Id</th>
                <th>Menu Name</th>
                <th>Menu Location</th>
                <th>Publication Status</th>
                <th>Action</th>
                </thead>
                <tbody>
                    <?php foreach ($select_menu as $v_menu) { ?>
                        <tr>
                            <td><?php echo $v_menu->menu_id ?></td>
                            <td><?php echo $v_menu->menu_name ?></td>
                            <td>
                                <?php 
                                if ($v_menu->location == 1) {
                                    echo 'Top';
                                } else {
                                    echo 'Footer';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($v_menu->pub_status == 1) {
                                    echo 'Published';
                                } else {
                                    echo 'Un Published';
                                }
                                ?>
                            </td>
                            <td>   <?php
                                if ($v_menu->pub_status == 1) {
                                    ?>
                                    <a class="btn btn-default" href="<?php echo base_url(); ?>super_admin/un_published_menu/<?php echo $v_menu->menu_id ?>">
                                        <i class="halflings-icon off" title="Published"></i>
                                    </a>
                                <?php } else { ?>
                                    <a class="btn btn-hover" href="<?php echo base_url(); ?>super_admin/published_menu/<?php echo $v_menu->menu_id ?>">
                                        <i class="halflings-icon ok" title="Un Published"></i>
                                    </a>
                                <?php } ?>
                                <a class="btn btn-info" href="<?php echo base_url(); ?>super_admin/edit_menu/<?php echo $v_menu->menu_id ?>">
                                    <i class="halflings-icon edit" title="Edit"></i>
                                </a>
                                <script type="text/javascript">
                                    function chkdelete() {
                                        var chk = confirm('Are you sure ?');
                                        if (chk) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    }
                                </script>
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>super_admin/delete_menu/<?php echo $v_menu->menu_id ?>">
                                    <i class="halflings-icon remove-sign" title="Delete" onclick="return chkdelete();"></i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>  

        </div>
    </div><!--/span-->

</div><!--/row-->



