<p class="panel">
    <?php
    $msg = $this->session->userdata('message');
    if ($msg) {
        ?>
        <span style="color: red; margin-left: 40%;font-size: 24px">
            <?php
            echo $msg;
            $this->session->unset_userdata('message')
            ?>
        </span>
    <?php } ?>
</p>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add Contact Info</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="super_admin/save_contact" method="post" enctype="multipart/form-data">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="box1">Contact Title</label>
                        <div class="controls">
                            <input type="text" name="title" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4"> <!--data-source='["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Dakota","North Carolina","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]'-->
<!--                            <p class="help-block">Start typing to activate auto complete!</p>-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="box1">Address </label>

                        <div class="controls">
                            <textarea class="cleditor" name="address" id="textarea2" rows="3"></textarea>
                        </div>

                    </div>   
                    <div class="control-group">
                        <label class="control-label" for="box1">Map</label>
                        <div class="controls">
                            <input type="text" name="map" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4">
                        </div>
                    </div>   
                    <div class="control-group">
                        <label class="control-label" for="box1">Cell </label>
                        <div class="controls">
                            <input type="text" name="cell" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4">
                        </div>
                    </div>   
                    <div class="control-group">
                        <label class="control-label" for="box1">Email </label>
                        <div class="controls">
                            <input type="text" name="email" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4">
                        </div>
                    </div>   
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->
