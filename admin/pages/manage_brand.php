<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Manage Catagory</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
                        <h2 class="pull-right"><span class="break"></span><a href="<?php echo base_url()?>super_admin/add_brand" class="btn btn-info">Add New Brand</a></h2>
            <table class="table table-responsive table-bordered ">
                <thead class="text-center">
                <th>Brand Id</th>
                <th>Brand Name</th>
                <th>Publication Status</th>
                <th>Action</th>
                </thead>
                <tbody>
                    <?php foreach ($select_brand as $v_brand) { ?>
                        <tr>
                            <td><?php echo $v_brand->brand_id ?></td>
                            <td><?php echo $v_brand->brand_name ?></td>
                            <td>
                                <?php
                                if ($v_brand->pub_status == 1) {
                                    echo 'Published';
                                } else {
                                    echo 'Un Published';
                                }
                                ?>
                            </td>
                            <td>   <?php
                                if ($v_brand->pub_status == 1) {
                                    ?>
                                    <a class="btn btn-default" href="<?php echo base_url(); ?>super_admin/un_published_brand/<?php echo $v_brand->brand_id ?>">
                                        <i class="halflings-icon off" title="Published"></i>
                                    </a>
                                <?php } else { ?>
                                    <a class="btn btn-hover" href="<?php echo base_url(); ?>super_admin/published_brand/<?php echo $v_brand->brand_id?>">
                                        <i class="halflings-icon ok" title="Un Published"></i>
                                    </a>
                                <?php } ?>
                                <a class="btn btn-info" href="<?php echo base_url(); ?>super_admin/edit_brand/<?php echo $v_brand->brand_id ?>">
                                    <i class="halflings-icon edit" title="Edit"></i>
                                </a>
                                <script type="text/javascript">
                                    function chkdelete() {
                                        var chk = confirm('Are you sure ?');
                                        if (chk) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    }
                                </script>
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>super_admin/delete_brand/<?php echo $v_brand->brand_id ?>">
                                    <i class="halflings-icon remove-sign" title="Delete" onclick="return chkdelete();"></i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>  

        </div>
    </div><!--/span-->

</div><!--/row-->



