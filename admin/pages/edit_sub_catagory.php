<p class="panel">
    <?php
    $msg = $this->session->userdata('message');
    if ($msg) {
        ?>
        <span style="color: red; margin-left: 40%;font-size: 24px">
            <?php
            echo $msg;
            $this->session->unset_userdata('message')
            ?>
        </span>
    <?php } ?>
</p>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Update Sub Catagory</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" name="sub_catagory" action="super_admin/update_sub_catagory" method="post" enctype="multipart/form-data">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="box1">Sub Catagory Name </label>
                        <div class="controls">
                            <input type="hidden" name="sub_id" value="<?php echo $select_sub_catagory_by_id->sub_id?>" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4" <!--data-source='["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Dakota","North Carolina","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]'-->
                            <input type="text" name="sub_name" value="<?php echo $select_sub_catagory_by_id->sub_name?>" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4" <!--data-source='["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Dakota","North Carolina","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]'-->
<!--                            <p class="help-block">Start typing to activate auto complete!</p>-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="box1">Sub Catagory Des </label>
                        <div class="controls">
                            <textarea class="cleditor" name="sub_des" id="textarea2" rows="3">
                                <?php echo $select_sub_catagory_by_id->sub_des?>
                            </textarea>
                        </div>
                    </div>   
                    <!--                    <div class="control-group">
                                            <label class="control-label" for="date01">Date input</label>
                                            <div class="controls">
                                                <input type="text" class="input-xlarge datepicker" id="date01" value="02/16/12">
                                            </div>
                                        </div>-->

                    <div class="control-group">
                        <label class="control-label">Select Your Catagory</label> 
                        <div class="controls">
                            <select id="selectError" name="cat_id">
                                <option>select catagory </option>
                                <?php foreach ($select_catagory as $v_catagory) { ?>
                                    <option value="<?php echo $v_catagory->cat_id?>"><?php echo $v_catagory->cat_name?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>        
                    <div class="control-group">
                        <label class="control-label">Publication status</label> 
                        <div class="controls">
                            <select id="selectError" name="pub_status" data-rel="chosen">
                                <option>select publication status</option>
                                <option value="1">Published</option>
                                <option value="0">Un_Published</option>
                            </select>
                        </div>
                    </div>          
                    <!--                    <div class="control-group hidden-phone">
                                            <label class="control-label" for="textarea2">Textarea WYSIWYG</label>
                                            <div class="controls">
                                                <textarea class="cleditor" id="textarea2" rows="3"></textarea>
                                            </div>
                                        </div>-->
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->
<script type="text/javascript">
    document.forms['sub_catagory'].elements['pub_status'].value='<?php echo $select_sub_catagory_by_id->pub_status;?>'
    document.forms['sub_catagory'].elements['cat_id'].value='<?php echo $select_sub_catagory_by_id->cat_id;?>'
</script>