<?php
if (isset($_POST['btn'])) {
    $obj->save_admin($_POST);
    unset($_POST['admin_name']);
    unset($_POST['email_address']);
    unset($_POST['password']);
    unset($_POST['access_level']);
}
?>

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add New Admin</h2>
            <div class="box-icon">
                <a href="#" id="re" class="">Manage Admin</a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                <fieldset>

                    <div class="control-group">
                        <?php
                        if (isset($_SESSION['msg'])) {
                            ?>
                            <h4 style="color: green">
                                <?php
                                echo $_SESSION['msg'];
                                unset($_SESSION['msg']);
                            } else {
                                echo "";
                            }
                            ?>
                            <label class="control-label" for="box1">Admin Name </label>
                            <div class="controls">
                                <input type="text" name="admin_name" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4" >
    <!--                            <p class="help-block">Start typing to activate auto complete!</p>-->
                            </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="box1">Admin Email Address </label>
                        <div class="controls">
                            <input type="email" name="email_address" required autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4">

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="box1">Admin Password </label>
                        <div class="controls">
                            <input type="password"  name="password" onchange="form.re_password = this.value;" required autofocus class="span6 typeahead" id="boxs"  data-provide="typeahead" data-items="4"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="box1">Re-type Password </label>
                        <div class="controls">
                            <input type="password"  name="re_password" onkeyup="passmatch()" required autofocus class="span6 typeahead" id="box2"  data-provide="typeahead" data-items="4"/>
                        </div>
                    </div>      <span id="pm"></span>
                    <script type="text/javascript">
                        function passmatch() {
                            var pas1 = document.getElementById('boxs').value;
                            var pass2 = document.getElementById('box2').value;
                            var match = "Password Match !!"
                            var Dmatch = "Password don't match"
                            if (pas1 == pass2) {
                                document.getElementById('pm').innerHTML = match;
                                document.getElementById('pm').style.color = "green";
                            }
                            else {
                                document.getElementById('pm').innerHTML = Dmatch;
                                document.getElementById('pm').style.color = "red";
                            }
                        }
                    </script>

                    <div class="control-group">
                        <label class="control-label">Access Level</label> 
                        <div class="controls">
                            <select id="selectError" name="access_level" >
                                <option>select access level</option>
                                <option value="1">Full Access</option>
                                <option value="0">Medium Access</option>
                            </select>
                        </div>
                    </div> 
                    <div class="form-actions">
                        <button type="submit"name="btn" class="btn btn-primary">Save changes</button>
                        <button type="reset"  class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->


