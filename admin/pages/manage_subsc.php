<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Manage Subscriber</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-responsive table-bordered ">

                <thead class="text-center">
                <th>Subscribe Id</th>
                <th>Email Address</th>
                <th>Date</th>
                <th>Action</th>
                </thead>
                <tbody>
                    <?php foreach ($total_subscribers as $v_subsc) { ?>
                        <tr>
                            <td><?php echo $v_subsc->subscribe_id ?></td>
                            <td><?php echo $v_subsc->email_address ?></td>
                            <td><?php echo $v_subsc->date ?></td>

                            <td>   
                                <script type="text/javascript">
                                    function chkdelete() {
                                        var chk = confirm('Are you sure ?');
                                        if (chk) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    }
                                </script>
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>super_admin/delete_subscribe/<?php echo $v_subsc->subscribe_id ?>">
                                    <i class="halflings-icon remove-sign" title="Delete" onclick="return chkdelete();"></i>
                                </a>
           <span class="newsletter"> <a href="super_admin/newsletter/<?php echo $v_subsc->subscribe_id ?>" >Send a newsletter</a></span>
                            
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>  
 <ul class="pagination-centered">
                <?php
                echo $this->pagination->create_links();
                ?>
            </ul>
        </div>
    </div><!--/span-->

</div><!--/row-->



