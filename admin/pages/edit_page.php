<p class="panel">
    <?php
    $msg = $this->session->userdata('message');
    if ($msg) {
        ?>
        <span style="color: red; margin-left: 40%;font-size: 24px">
            <?php
            echo $msg;
            $this->session->unset_userdata('message')
            ?>
        </span>
    <?php } ?>
</p>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Edit Page</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" name="page" action="super_admin/update_page" method="post" enctype="multipart/form-data">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="box1">Page Title </label>
                        <div class="controls">
                            <input type="text" value="<?php echo $select_page_by_id->page_title ?>" name="page_title" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4" <!--data-source='["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Dakota","North Carolina","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]'-->
                                   <input type="hidden" value="<?php echo $select_page_by_id->page_id ?>" name="page_id" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4" <!--data-source='["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Dakota","North Carolina","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]'-->
<!--                            <p class="help-block">Start typing to activate auto complete!</p>-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="box1">Page Details </label>
                        <div class="controls">
                            <textarea class="cleditor" name="page_details" id="textarea2" rows="3"><?php echo $select_page_by_id->page_details ?></textarea>
                        </div>
                    </div>  
                    <div class="control-group">
                        <label class="control-label" for="fileInput">Page Image</label>
                        <div class="controls">
                            <input class="input-file uniform_on" name="page_image" id="fileInput" type="file">
                            <img src="<?php echo base_url() . $select_page_by_id->page_image ?>" width="100" heigth="100">

                            <p class="help-block">Image max 1024px and height max 768px and size maximum 3Mb</p>
                        </div>
                    </div>   
                    <div class="control-group">
                        <label class="control-label" for="box1">Page Parent</label>
                        <div class="controls">
                            <select id="selectError" name="menu_id" >
                                <option>select Menu</option>
                                <?php foreach ($select_menu as $v_menu) { ?>
                                    <option value="<?php echo $v_menu->menu_id ?>"><?php echo $v_menu->menu_name ?></option>
                                <?php } ?>
                            </select>
                        </div>   
                    </div>   

                    <div class="control-group">
                        <label class="control-label">Publication status</label> 
                        <div class="controls">
                            <select id="selectError" name="pub_status" data-rel="chosen">
                                <option>select publication status</option>
                                <option value="1">Published</option>
                                <option value="0">Un_Published</option>
                            </select>
                        </div>
                    </div>          
                    <!--                    <div class="control-group hidden-phone">
                                            <label class="control-label" for="textarea2">Textarea WYSIWYG</label>
                                            <div class="controls">
                                                <textarea class="cleditor" id="textarea2" rows="3"></textarea>
                                            </div>
                                        </div>-->
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->
<script type="text/javascript">
    document.forms['page'].elements['menu_id'].value = '<?php echo $select_page_by_id->menu_id ?>';
    document.forms['page'].elements['pub_status'].value = '<?php echo $select_page_by_id->pub_status ?>';
</script>
