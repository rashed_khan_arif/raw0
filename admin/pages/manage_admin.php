<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>ManageAdmin</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <h2 class="pull-right"><span class="break"></span><a href="<?php echo base_url()?>super_admin/add_admin" class="btn btn-info">Add New Admin</a></h2>
            <table class="table table-responsive table-bordered ">
                <thead class="text-center">
                <th>Admin Id</th>
                <th>Admin Name</th>
                <th>Admin Email Address</th>
               <!-- <th>Admin Image</th>-->
                <th>Access Level</th>
                <th>Action</th>
                </thead>
                <tbody>
                    <?php foreach ($select_admin as $v_admin) { ?>
                        <tr>
                            <td><?php echo $v_admin->admin_id ?></td>
                            <td><?php echo $v_admin->admin_name ?></td>
                            <td><?php echo $v_admin->email_address ?></td>
                          <!-- <td><img src="<?php //echo base_url() . $v_admin->admin_image ?>" width="60" height="50" style="margin-left: 40%"/></td>-->
                            <td>
                                <?php
                                if ($v_admin->access_level == 1) {
                                    echo 'Super Admin(Full Access)';
                                } else {
                                    echo 'Medium access';
                                }
                                ?>
                            </td>
                            <td>  
                                <a class="btn btn-info" href="<?php echo base_url(); ?>super_admin/edit_admin/<?php echo $v_admin->admin_id ?>">
                                    <i class="halflings-icon edit" title="Edit"></i>
                                </a>
                                <script type="text/javascript">
                                    function chkdelete() {
                                        var chk = confirm('Are you sure ?');
                                        if (chk) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    }
                                </script>
                                <?php
                                $admin = $this->session->userdata('admin_id');
                                if ($v_admin->admin_id != $admin) {
                                    ?>
                                    <a class="btn btn-danger" href="<?php echo base_url(); ?>super_admin/delete_admin/<?php echo $v_admin->admin_id ?>">
                                        <i class="halflings-icon remove-sign" title="Delete" onclick="return chkdelete();"></i>
                                    </a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>  

        </div>
    </div><!--/span-->

</div><!--/row-->



