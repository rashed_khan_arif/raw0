<p class="panel">
    <?php
    $msg = $this->session->userdata('message');
    if ($msg) {
        ?>
        <span style="color: red; margin-left: 40%;font-size: 24px">
            <?php
            echo $msg;
            $this->session->unset_userdata('message')
            ?>
        </span>
    <?php } ?>
</p>

<script src="design/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var xmlhttp = false;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }

    function makerequest(given_value, objID)
    {
        //var obj = document.getElementById(objID);
        //alert(objID);
        serverPage = 'super_admin/sub_cat_check/' + given_value;
        // alert(serverPage);
        xmlhttp.open("GET", serverPage);
        xmlhttp.onreadystatechange = function ()
        {
            //alert(xmlhttp.readyState);
            //alert(xmlhttp.status);
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById(objID).innerHTML = xmlhttp.responseText;
                //document.getElementById(objcw).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.send(null);
    }
</script>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add New Product</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div> <h2><span class="break"></span><a href="super_admin/manage_product" class="btn btn-info">Manage Product</a></h2>
        <div class="box-content">
         
            <form class="form-horizontal" action="super_admin/save_product" method="post" enctype="multipart/form-data">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="box1">Product Name </label>
                        <div class="controls">
                            <input type="text" name="product_name" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4" >
<!--                            <p class="help-block">Start typing to activate auto complete!</p>-->
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="box1">Product Description</label>
                        <div class="controls">
                            <textarea class="cleditor" name="product_des" id="textarea2" rows="3"></textarea>
                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="box1">Product Code </label>
                        <div class="controls">
                            <input type="text" name="product_code" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4"
                        </div>
                    </div>   
                    </div>   
                    <div class="control-group">
                        <label class="control-label" for="box1">Product Price</label>
                        <div class="controls">
<!--                            <select id="selectError" name="product_price">
                                <option>select</option>
                                <option value="1">Call for price</option>
                            </select><label>Or</label>
                            <div style="clear:both"></div>-->
                            <input type="text" name="product_price" placeholder="type price amount or anything" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4"
                        </div>
                    </div>   
                    </div>   
                    <div class="control-group">
                        <label class="control-label" for="box1">Availability</label>
                        <div class="controls">
                            <input type="text" name="availability" autofocus class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4"
                        </div>
                    </div>   
                    </div>   
                    <div class="control-group">
                        <label class="control-label" for="box1">Condition</label>
                        <div class="controls">
                            <textarea class="cleditor" name="condition" id="textarea2" rows="3"></textarea>
                        </div>
                    </div>   


                    <div class="control-group">
                        <label class="control-label">Featured Product</label>
                        <div class="controls">
                            <label class="radio">
                                <input type="checkbox" name="featured_product" id="optionsRadios1">
                                Yes
                            </label> 
                        </div>
                    </div> 
                     <div class="control-group">
                        <label class="control-label" for="box1">Product Status</label>
                        <div class="controls">
                            <input type="text" name="status" class="span6 typeahead" id="box1"  data-provide="typeahead" data-items="4"
                        </div>
                    </div>   
                    </div> 
                    <div class="control-group">
                        <label class="control-label">Product Catagory(*)</label> 
                        <div class="controls">
                            <select id="selectError" required onchange="makerequest(this.value, 'msg')" name="cat_id">
                                <option value="">select catagory</option>
                                <?php foreach ($select_catagory as $v_catagory) { ?>
                                    <option value="<?php echo $v_catagory->cat_id; ?>"><?php echo $v_catagory->cat_name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="box1">Sub Catagory(*)</label>
                        <div class="controls">
                            <select  id="msg" required  name="sub_id">

                            </select>
                        </div>
                    </div> 
                 
                    <div class="control-group">
                        <label class="control-label">Product Brand(*)</label> 
                        <div class="controls">
                            <select id="selectError" required name="brand_id">
                                <option value="">select brand</option>
                                <?php foreach ($select_brand as $v_brand) { ?>
                                    <option value="<?php echo $v_brand->brand_id; ?>"><?php echo $v_brand->brand_name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div> 
                 
                    <div class="control-group">
                        <label class="control-label" for="fileInput">Product Image(*)</label>
                        <div class="controls">
                            <input class="input-file uniform_on" name="product_image" onchange="readURL(this);" id="fileInput" type="file">
                             <img src="" id="image" width="50" heigth="50">
                            <p class="help-block" style="color: green">Image must be Width 253px and height 188px and size maximum 150 kb</p>
                        </div>
                    </div>   
                        <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
                            <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>

                            <script>
                                        function readURL(input) {
                                            if (input.files && input.files[0]) {
                                                var reader = new FileReader();

                                                reader.onload = function (e) {
                                                    $('#image')
                                                            .attr('src', e.target.result)
                                                           
                                                            .width(253)
                                                            .height(188);
                                                };

                                                reader.readAsDataURL(input.files[0]);
                                            }
                                        }
                            </script>
                    <div class="control-group">
                        <label class="control-label" for="fileInput">Product Image For Zoom</label>
                        <div class="controls">
                            <input class="input-file uniform_on" name="product_image2" id="fileInput" type="file">
                          
                        </div>
                    </div>   
                    <div class="control-group">
                        <label class="control-label">Publication status</label> 
                        <div class="controls">
                            <select id="selectError" name="pub_status">
                                <option>select publication status</option>
                                <option value="1">Published</option>
                                <option value="0">Un_Published</option>
                            </select>
                        </div>
                    </div>  
                
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->
