<?php
session_start();
require_once './class/admin.php';
$obj = new Admin();
if (array_key_exists('admin_id', $_SESSION)) {
    $id = $_SESSION['admin_id'];
}
if (!isset($id)) {
    header('Location:index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- start: Meta -->
        <meta charset="utf-8">
        <title>Bootstrap Metro Dashboard by Dennis Ji for ARM demo</title>
        <meta name="description" content="Bootstrap Metro Dashboard">
        <meta name="author" content="Dennis Ji">
        <meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link id="bootstrap-style" href="../design/css/bootstrap.min.css" rel="stylesheet">
        <link href="../design/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link id="base-style" href="../design/css/style.css" rel="stylesheet">
        <link id="base-style-responsive" href="../design/css/style-responsive.css" rel="stylesheet">
        <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>-->

        <link rel="shortcut icon" href="../design/img/favicon.ico">

    </head>

    <body>
        <!-- start: Header -->
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="index.html"><span>Metro</span></a>

                    <!-- start: Header Menu -->
                    <div class="nav-no-collapse header-nav">
                        <ul class="nav pull-right"> 
                            <li class="dropdown">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="halflings-icon white user"></i> <?php echo $_SESSION['admin_name'] ?>
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-menu-title">
                                        <span>Account Settings</span>
                                    </li>
                                    <li><a href="#"><i class="halflings-icon user"></i> Profile</a></li>
                                    <li><a href="login.html"><i class="halflings-icon off"></i> Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- start: Header -->

        <div class="container-fluid-full">
            <div class="row-fluid">
                <!-- start: Main Menu -->
                <div id="sidebar-left" class="span2" style="overflow:scroll;">
                    <div class="nav-collapse sidebar-nav">
                        <ul class="nav nav-tabs nav-stacked main-menu">
                            <li><a href="#"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a></li>	
                            <li><a href="_add_admin.php"><span class="hidden-tablet"> Add Admin</span></a></li>
                            <li><a href="tasks.html"><i class="icon-tasks"></i><span class="hidden-tablet"> Tasks</span></a></li>
                            <li><a href="ui.html"><i class="icon-eye-open"></i><span class="hidden-tablet"> UI Features</span></a></li>
                            <li><a href="widgets.html"><i class="icon-dashboard"></i><span class="hidden-tablet"> Widgets</span></a></li>
                            <li>
                                <a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Dropdown</span></a>
                                <ul>
                                    <li><a class="submenu" href="submenu.html"><i class="icon-file-alt"></i><span class="hidden-tablet"> Sub Menu 1</span></a></li>
                                    <li><a class="submenu" href="submenu2.html"><i class="icon-file-alt"></i><span class="hidden-tablet"> Sub Menu 2</span></a></li>
                                    <li><a class="submenu" href="submenu3.html"><i class="icon-file-alt"></i><span class="hidden-tablet"> Sub Menu 3</span></a></li>
                                </ul>	
                            </li>
                            <li><a href="form.html"><i class="icon-edit"></i><span class="hidden-tablet"> Forms</span></a></li>
                        </ul>
                    </div>
                </div>
                <!-- end: Main Menu -->

                <!-- start: Content -->
                <div id="content" class="span10">


                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="">Home</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li><a href="#">Dashboard</a></li>
                    </ul>

                    <!--/row-->
                    <?php
                    if (isset($page)) {
                        if ($page == 'add_admin.php') {
                            include './pages/add_admin.php';
                        }
                    } else {
                        include './home.php';
                    }
                    ?>

                </div><!--/.fluid-container-->

                <!-- end: Content -->
            </div><!--/#content.span10-->
        </div><!--/fluid-row-->

        <div class="modal hide fade" id="myModal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3>Settings</h3>
            </div>
            <div class="modal-body">
                <p>Here settings can be configured...</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal">Close</a>
                <a href="#" class="btn btn-primary">Save changes</a>
            </div>
        </div>

        <div class="clearfix"></div>

        <footer>

            <p>
                <span style="text-align:left;float:left">&copy; 2013 <a href="http://jiji262.github.io/Bootstrap_Metro_Dashboard/" alt="Bootstrap_Metro_Dashboard">Bootstrap Metro Dashboard</a></span>

            </p>

        </footer>

        <!-- start: JavaScript-->

        <script src="../design/js/jquery-1.9.1.min.js"></script>
        <script src="../design/js/jquery-migrate-1.0.0.min.js"></script>

        <script src="../design/js/jquery-ui-1.10.0.custom.min.js"></script>

        <script src="../design/js/jquery.ui.touch-punch.js"></script>

        <script src="../design/js/modernizr.js"></script>

        <script src="../design/js/bootstrap.min.js"></script>

        <script src="../design/js/jquery.cookie.js"></script>

        <script src='../design/js/fullcalendar.min.js'></script>

        <script src='../design/js/jquery.dataTables.min.js'></script>

        <script src=../design/"js/excanvas.js"></script>
        <script src="../design/js/jquery.flot.js"></script>
        <script src="../design/js/jquery.flot.pie.js"></script>
        <script src="../design/js/jquery.flot.stack.js"></script>
        <script src="../design/js/jquery.flot.resize.min.js"></script>

        <script src="../design/js/jquery.chosen.min.js"></script>

        <script src="../design/js/jquery.uniform.min.js"></script>

        <script src="../design/js/jquery.cleditor.min.js"></script>

        <script src="../design/js/jquery.noty.js"></script>

        <script src="../design/js/jquery.elfinder.min.js"></script>

        <script src="../design/js/jquery.raty.min.js"></script>

        <script src="../design/js/jquery.iphone.toggle.js"></script>

        <script src="../design/js/jquery.uploadify-3.1.min.js"></script>

        <script src="../design/js/jquery.gritter.min.js"></script>

        <script src="../design/js/jquery.imagesloaded.js"></script>

        <script src="../design/js/jquery.masonry.min.js"></script>

        <script src="../design/js/jquery.knob.modified.js"></script>

        <script src="../design/js/jquery.sparkline.min.js"></script>

        <script src="../design/js/counter.js"></script>

        <script src="../design/js/retina.js"></script>

        <script src="../design/js/custom.js"></script>
        <!-- end: JavaScript-->

    </body>
</html>
