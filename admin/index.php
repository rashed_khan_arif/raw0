<?php
require './class/login.php';
$login = new Login();
if (isset($_POST['btn'])) {
    $msg = $login->check_login($_POST);
}
if (array_key_exists('admin_id', $_SESSION)) {
    $id = $_SESSION['admin_id'];
}
if (isset($id)) {
    header('Location:dashboard.php');
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <meta name="description" content="Bootstrap Metro Dashboard">
        <meta name="author" content="Dennis Ji">
        <meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link id="bootstrap-style" href="../design/css/bootstrap.min.css" rel="stylesheet">
        <link href="../design/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link id="base-style" href="../design/css/style.css" rel="stylesheet">
        <link id="base-style-responsive" href="../design/css/style-responsive.css" rel="stylesheet">
        <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>-->

        <link rel="shortcut icon" href="../design/img/favicon.ico">
        <style type="text/css">
            /*body { background: url(../design/img/bg-login.jpg) !important; }*/
        </style>
    </head>

    <body>
        <div class="container-fluid-full">
            <div class="row-fluid">

                <div class="row-fluid">
                    <div class="login-box">  
                        <center>
                            <h2 style="color:red">
                                <?php
                                if (isset($msg)) {
                                    echo $msg;
                                    unset($msg);
                                }
                                ?>
                            </h2>
                        </center>
                        <form class="form-horizontal" action="" method="post">

                            <div class="input-prepend" title="Email Address">
                                <span class="add-on"><i class="halflings-icon user"></i></span>
                                <input class="input-large span10" name="email_address" required="required" id="username" type="email" placeholder="type email address"/>
                            </div>
                            <div class="clearfix"></div>
                            <div class="input-prepend" title="Password">
                                <span class="add-on"><i class="halflings-icon lock"></i></span>
                                <input class="input-large span10" name="password" required="required" id="password" type="password" placeholder="type password"/>
                            </div>
                            <div class="clearfix"></div>



                            <div class="button-login">	
                                <button type="submit" name="btn" class="btn btn-primary">Login</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                        <hr>
                        <h3 id="msg"></h3>
                        <h3>Forgot Password?</h3>
                        <p>
                            No problem, <a href="admin_login/reset_password">click here</a> to get a new password.
                        </p>
                    </div><!--/span-->
                </div><!--/row-->


            </div><!--/.fluid-container-->

        </div><!--/fluid-row-->

        <!-- start: JavaScript-->

        <script src="design/js/jquery-1.9.1.min.js"></script>
        <script src="design/js/jquery-migrate-1.0.0.min.js"></script>

        <script src="design/js/jquery-ui-1.10.0.custom.min.js"></script>

        <script src="design/js/jquery.ui.touch-punch.js"></script>

        <script src="design/js/modernizr.js"></script>

        <script src="design/js/bootstrap.min.js"></script>

        <script src="design/js/jquery.cookie.js"></script>

        <script src='design/js/fullcalendar.min.js'></script>

        <script src='design/js/jquery.dataTables.min.js'></script>

        <script src="design/js/excanvas.js"></script>
        <script src="design/js/jquery.flot.js"></script>
        <script src="design/js/jquery.flot.pie.js"></script>
        <script src="design/js/jquery.flot.stack.js"></script>
        <script src="design/js/jquery.flot.resize.min.js"></script>

        <script src="design/js/jquery.chosen.min.js"></script>

        <script src="design/js/jquery.uniform.min.js"></script>

        <script src="design/js/jquery.cleditor.min.js"></script>

        <script src="design/js/jquery.noty.js"></script>

        <script src="design/js/jquery.elfinder.min.js"></script>

        <script src="design/js/jquery.raty.min.js"></script>

        <script src="design/js/jquery.iphone.toggle.js"></script>

        <script src="design/js/jquery.uploadify-3.1.min.js"></script>

        <script src="design/js/jquery.gritter.min.js"></script>

        <script src="design/js/jquery.imagesloaded.js"></script>

        <script src="design/js/jquery.masonry.min.js"></script>

        <script src="design/js/jquery.knob.modified.js"></script>

        <script src="design/js/jquery.sparkline.min.js"></script>

        <script src="design/js/counter.js"></script>

        <script src="design/js/retina.js"></script>

        <script src="design/js/custom.js"></script>
        <!-- end: JavaScript-->

    </body>
</html>
